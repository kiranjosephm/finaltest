package finalTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		ArrayList<Object> sh = new ArrayList<>();
		
		while (choice != 3)
		{
			// 1. show the menu
			showMenu();
	
			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();

		
			// 3. DEBUG: Output what the user typed in 
			System.out.println("You entered: " + choice);
			System.out.println();
			switch(choice)
			{
			case 1:
				double base,height;
				System.out.println("Enter base: ");
				base = keyboard.nextDouble();
				System.out.println("Enter Height: ");
				height = keyboard.nextDouble();
				triangle t= new triangle(base,height);
				System.out.println(t.printInfo());
				sh.add(t.printInfo());
				break;
			case 2:
				double side;
				System.out.println("Enter side: ");
				side = keyboard.nextDouble();
				square s=new square(side);
				System.out.println(s.printInfo());
				sh.add(s.printInfo());
				break;
			case 3:
				break;
			default:
				System.out.println("INVALID CHOICE");
				
			}
		}
		keyboard.close();
		for(int i=0;i<sh.size();i++)
		{
			System.out.println(sh.get(i));
		}
	}
		
	
	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Exit");
	}

}
