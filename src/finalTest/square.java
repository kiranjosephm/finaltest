package finalTest;

public class square extends shapes implements TwoDimensionalShapeInterface
{

	private double side;
	
	public square(double side)
	{
		this.side=side;
		this.setcolor();
	}

	public double getSide() 
	{
		return side;
	}

	public void setSide(double side) 
	{
		this.side = side;
	}
	
	public double calculateArea()
	{
		return this.getSide()*this.getSide();
	}
	
	public void setcolor()
	{
		if(side<50.0)
			super.setColor("RED");
		else
			super.setColor("GREEN");
	}
	public String printInfo()
	{
		return super.getColor() +" SQUARE with side : "+this.getSide()+" and area : "+this.calculateArea();
	}
	
}
