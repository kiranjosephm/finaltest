package finalTest;

public class triangle extends shapes implements TwoDimensionalShapeInterface
{
	private double base;
	private double height;
	
	public triangle(double base,double height)
	{
		this.base=base;
		this.height=height;
		this.setcolor();
	}
	
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	
	public void setcolor()
	{
		if(base<50.0)
			super.setColor("BLUE");
		else
			super.setColor("YELLOW");
	}
	
	public double calculateArea()
	{
		return this.getBase()*this.getHeight()*0.5;
	}
	
	public String printInfo()
	{
		return super.getColor() +" TRIANGLE with base : "+this.getBase()+" , height : "+this.getHeight()+" and AREA :"+this.calculateArea();
	}

}
